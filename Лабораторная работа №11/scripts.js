var app = new Vue({
	el: '#student_list',
	data: {
		full_name:[],
		name: null,
		second_name: null,
		color_variant: null
	},
	
	methods: {
		onClick(msg, event)
		{
			if(event) {
				event.preventDefault()
			}
			
			if(this.name && this.second_name) {
				this.full_name.push(this.name + " " + this.second_name),
				this.name = null;
				this.second_name = null
			}
		}
	}
})